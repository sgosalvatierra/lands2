﻿
namespace Lands.ViewsModels
{

    using System;
    using GalaSoft.MvvmLight.Command;
    using Services;
    using System.ComponentModel;
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;
    using Helpers;

    public class LoginViewModel : BaseViewModel
    {

        #region Servicios
        private ApiService apiService;
        #endregion

        #region Atributos
        private String password;
        private String email;
        private bool isrunning;
        private bool isenabled;
        #endregion

        #region Propiedades
        public string Email
        {
            get { return this.email; }
            set { SetValue(ref this.email, value); }
        }
        public string Password
        {
            get { return this.password; }
            set { SetValue(ref this.password, value); }
        }
        public bool IsRemembered
        {
            get;
            set;
        }
        public bool IsRunning
        {
            get { return this.isrunning; }
            set { SetValue(ref this.isrunning, value); }
        }
        public bool IsEnabled
        {
            get { return this.isenabled; }
            set { SetValue(ref this.isenabled, value); }
        }
        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        private async void Login()
        {
            if (String.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.EmailValidation,
                    Languages.Accept);
                return;
            }

            if (String.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Debes ingresar tu password.",
                    "Aceptar");
                return;
            }
            this.IsRunning = true;
            this.IsEnabled = false;

            var conexion = await apiService.CheckConnection();
            if (!conexion.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                                    "Error",
                                    conexion.Message,
                                    "Aceptar");
                return;
            }

            var token = await apiService.GetToken(
                "https://landsapitut.azurewebsites.net",
                this.Email,
                this.Password);

            if (token == null)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                                    "Error",
                                    "Algo sucedio mal. Por favor intenta nuevamente",
                                    "Aceptar");
                return;
            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                                    "Error",
                                    token.ErrorDescription,
                                    "Aceptar");
                this.Password = string.Empty;
                return;
            }

            //if (this.Email != "jzuluaga55@gmail.com" || this.Password != "1234")
            //{
            //    this.IsRunning = false;
            //    this.IsEnabled = true;
            //    await Application.Current.MainPage.DisplayAlert(
            //        "Error",
            //        "Password o Email incorrecto.",
            //        "Aceptar");
            //    this.Password = String.Empty;
            //    return;
            //}

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Token = token;
            mainViewModel.Lands = new LandsViewModel();
            ///esto de arriba es para instanciar la LandsViewModel y ademas 
            ///implementamos un patron Singleton para evitar generar una nueva instancia de MainViewModel
            ///es decir debemos tener la unica instancia mientras dure todo el proyecto
            await Application.Current.MainPage.Navigation.PushAsync(new LandsPage());


            this.IsRunning = false;
            this.IsEnabled = true;

            this.Email = string.Empty;
            this.Password = string.Empty;

                        
        }
        #endregion

        #region Constructores
        public LoginViewModel()
        {
            this.apiService = new ApiService();

            this.IsRemembered = true;
            this.IsEnabled = true;

            this.Email = "juan@gmail.com";
            this.Password = "123456";
        }
        #endregion

    }
}
